<div align="center">
<p>
    <a href="https://operatorict.cz">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/core/-/raw/development/.assets/oict_logo.png" alt="oict" width="100px" height="100px" />
    </a>
    <a href="https://golemio.cz">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/core/-/raw/development/.assets/golemio_logo.png" alt="golemio" width="100px" height="100px" />
    </a>
</p>

<h1>@golemio/db-common</h1>

<p>
    <a href="https://gitlab.com/operator-ict/golemio/code/modules/db-common/commits/master">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/db-common/badges/master/pipeline.svg" alt="pipeline">
    </a>
    <a href="./LICENSE">
        <img src="https://img.shields.io/npm/l/@golemio/db-common" alt="license">
    </a>
</p>

<p>
    <a href="#installation">Installation</a> · <a href="./docs">Documentation</a>
</p>
</div>

This module is intended for use with Golemio services. It only contains common database migrations and scripts.

## Installation

We recommend to install this module as an exact version.

```bash
# Production (Golem) migrations
npm install --save-exact @golemio/db-common@latest

# Development (Rabin) migrations
npm install --save-exact @golemio/db-common@dev
```

<!-- ## Description -->

<!-- Insert module-specific info here -->
