create table error_log (
	class_name varchar(255) NULL,
	id bigserial NOT NULL,
	info text NULL,
	message text NOT NULL,
	service varchar(255) NULL,
	stack_trace text NULL,
	status int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT error_log_pkey PRIMARY KEY (id)
);
