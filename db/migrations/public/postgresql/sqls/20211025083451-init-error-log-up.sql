-- error_log
CREATE SEQUENCE IF NOT EXISTS error_log_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

CREATE TABLE IF NOT EXISTS error_log (
    class_name character varying(255),
    id bigint DEFAULT nextval('error_log_id_seq'::regclass) NOT NULL,
    info text,
    message character varying(255) NOT NULL,
    service character varying(255),
    stack_trace text,
    status integer,
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT error_log_pkey PRIMARY KEY (id)
);
