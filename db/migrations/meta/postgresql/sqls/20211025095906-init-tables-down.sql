DROP TABLE IF EXISTS extract_summry;

DROP TABLE IF EXISTS batch_validation;
DROP SEQUENCE IF EXISTS batch_validation_id_seq;

DROP TABLE IF EXISTS batch;
DROP SEQUENCE IF EXISTS batch_id_batch_seq;

DROP TRIGGER IF EXISTS trg_extract ON extract;
DROP TABLE IF EXISTS extract;

DROP TRIGGER IF EXISTS trg_dataset ON dataset;
DROP TABLE IF EXISTS dataset;
DROP SEQUENCE IF EXISTS dataset_id_dataset_seq;

DROP TABLE IF EXISTS db_type;
DROP SEQUENCE IF EXISTS db_type_id_db_seq;
