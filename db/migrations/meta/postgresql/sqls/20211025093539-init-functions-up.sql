-- add_audit_fields
CREATE OR REPLACE FUNCTION add_audit_fields(
    p_table_schema character varying,
    p_table_name character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
AS $BODY$

declare
    sql varchar(5000);
begin
-- kontrola existence tabulky
    if not exists ( select table_name from information_schema.tables where upper(table_name) = upper(p_table_name) and upper(p_table_schema) = upper(table_schema)) then
        raise EXCEPTION 'Tabulka (%) neexistuje', p_table_name;
    end if;

-- /kontrola
-- create
    -- CREATE_BATCH_ID
    if not exists
    (
        select column_name from information_schema.columns
        where upper(table_name) = upper(p_table_name)
        and upper(p_table_schema) = upper(table_schema)
        and upper(column_name) = 'CREATE_BATCH_ID'
    )   then
    -- sloupec chybí - přidáme
        sql := 'alter table ' || p_table_schema||'.'||p_table_name || ' add CREATE_BATCH_ID bigint';
        EXECUTE sql;

        sql := 'COMMENT ON COLUMN ' ||  p_table_schema||'.'||p_table_name || '.CREATE_BATCH_ID is ''ID vstupní dávky''' ;
        EXECUTE sql;
    end if;
    -- /CREATE_BATCH_ID
    -- CREATED_at
    if not exists
    (
        select column_name from information_schema.columns where upper(table_name) = upper(p_table_name) and upper(p_table_schema) = upper(table_schema) and upper(column_name) = 'CREATED_AT'
    )   then

    -- sloupec chybí - přidáme
        EXECUTE 'alter table ' || p_table_schema||'.'||p_table_name || ' add CREATED_AT TIMESTAMP';
        EXECUTE 'COMMENT ON COLUMN ' ||  p_table_schema||'.'||p_table_name || '.CREATED_AT is ''Čas vložení''';

    end if;
    -- /CREATED_AT
    -- CREATED_BY
    if not exists
    (
        select column_name from information_schema.columns where upper(table_name) = upper(p_table_name) and upper(p_table_schema) = upper(table_schema) and upper(column_name) = 'CREATED_BY'
    )   then

    -- sloupec chybí - přidáme
        EXECUTE 'alter table ' ||  p_table_schema||'.'||p_table_name || ' add CREATED_BY varchar(150)';
        EXECUTE 'COMMENT ON COLUMN ' ||  p_table_schema||'.'||p_table_name || '.CREATED_BY is ''identikace uživatele/procesu, který záznam vložil''';

    end if;
    -- /CREATED_BY
-- /create
-- update
    -- UPDATE_BATCH_ID
    if not exists
    (
        select column_name from information_schema.columns where upper(table_name) = upper(p_table_name) and upper(p_table_schema) = upper(table_schema) and upper(column_name) = 'UPDATE_BATCH_ID'
    )   then

    -- sloupec chybí - přidáme
        EXECUTE 'alter table ' ||  p_table_schema||'.'||p_table_name || ' add UPDATE_BATCH_ID bigint';
        EXECUTE 'COMMENT ON COLUMN ' ||  p_table_schema||'.'||p_table_name || '.UPDATE_BATCH_ID is ''ID poslední dávky, která záznam modifikovala''' ;

    end if;
    -- /UPDATE_BATCH_ID
    -- UPDATED_AT
    if not exists
    (
        select column_name from information_schema.columns where upper(table_name) = upper(p_table_name) and upper(p_table_schema) = upper(table_schema) and upper(column_name) = 'UPDATED_AT'
    )   then

    -- sloupec chybí - přidáme
        EXECUTE 'alter table ' ||  p_table_schema||'.'||p_table_name || ' add UPDATED_AT TIMESTAMP';
        EXECUTE 'COMMENT ON COLUMN ' ||  p_table_schema||'.'||p_table_name || '.UPDATED_AT is ''Čas poslední modifikace''';

    end if;
    -- /UPDATED_AT
    -- UPDATED_BY
    if not exists
    (
        select column_name from information_schema.columns where upper(table_name) = upper(p_table_name) and upper(p_table_schema) = upper(table_schema) and upper(column_name) = 'UPDATED_BY'
    )   then

    -- sloupec chybí - přidáme
        EXECUTE 'alter table ' ||  p_table_schema||'.'||p_table_name || ' add UPDATED_BY varchar(150)';
        EXECUTE 'COMMENT ON COLUMN ' ||  p_table_schema||'.'||p_table_name || '.UPDATED_BY is ''Identikace uživatele/procesu, který poslední měnil záznam''';

    end if;
    -- /UPDATED_BY
-- /update

-- indexy
    if not exists
    (
        select indexname from pg_indexes
        where indexname  =  p_table_name || '_create_batch'
        and upper(p_table_schema) = upper(schemaname)
    )   then

        EXECUTE 'create index ' ||  p_table_name || '_create_batch on '|| p_table_schema||'.'||p_table_name|| '(CREATE_BATCH_ID)';
    end if;

    if not exists
    (
        select indexname from pg_indexes
        where indexname  =  p_table_name || '_update_batch'
        and upper(p_table_schema) = upper(schemaname)
    )   then

        sql := 'create index ' ||  p_table_name || '_update_batch on '|| p_table_schema||'.'||p_table_name|| '(UPDATE_BATCH_ID)';
        EXECUTE sql;
    end if;

-- /indexy

    return 0;
end;

$BODY$;

COMMENT ON FUNCTION add_audit_fields(character varying, character varying) IS 'Přidá do existující tabulky auditní sloupce';

-- close_batch
CREATE OR REPLACE FUNCTION close_batch(
    p_id_batch integer,
    p_statut character varying)
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE
AS $BODY$

declare
    pocet integer;
    xid_dataset int;
    xschema varchar(50);
    xtable varchar(150);
    query_rec record;
    query_rec1 record;
    xinsert integer;
    xupd integer;
    xstatut varchar(50);

begin
    -- zjištění vstupní sada
    select id_dataset,statut_batch into xid_dataset,xstatut
    from meta.batch
    where id_batch = p_id_batch;

    if xid_dataset is null then
        raise EXCEPTION 'Dávka (%) neexistuje, nebo nemá nastaven správný dataset', p_id_batch;
    end if;

    if not xstatut  =  'NEW' then
        raise EXCEPTION 'Dávka (%) není ve stavu NEW', p_id_batch;
    end if;

    if upper(p_statut) = 'ERROR' then
        update meta.batch
        set statut_batch = p_statut,
        end_batch = current_Timestamp
        where id_batch = p_id_batch;

        return;
    end if;

    FOR  query_rec IN
        select schema_extract,name_extract
        from meta.extract
        where id_dataset = xid_dataset and id_db = 1
    LOOP
        if not exists (SELECT * FROM meta.extract_summry WHERE name_extract=query_rec.name_extract and statut = 'OK' and id_batch = p_id_batch)
        then
            xschema := query_rec.schema_extract;
            xtable := query_rec.name_extract;
            select p_count_insert,p_count_upd into xinsert,xupd
            from meta.count_batch(xtable,xschema,p_id_batch);

            if xinsert+xupd >0 then

                insert into meta.extract_summry (id_batch, name_extract, statut, insert_count, update_count)
                    VALUES (p_id_batch, xtable, p_statut, xinsert, xupd)
                on conflict (id_batch, name_extract)
                do
                    update set statut=p_statut, insert_count = xinsert, update_count=xupd;
            end if;
        end if;
    END LOOP;

    update meta.batch
    set statut_batch = p_statut,
    end_batch = current_Timestamp
    where id_batch = p_id_batch;

end;

$BODY$;

COMMENT ON FUNCTION close_batch(integer, character varying) IS 'Fce uzavírá existující dávku';

-- close_extract
CREATE OR REPLACE FUNCTION close_extract(
    p_id_batch integer,
    p_extract character varying,
    p_insert integer,
    p_update integer,
    p_delete integer)
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE
AS $BODY$

declare
begin
    -- zjištění existence dávky a její statut
    if not exists (SELECT id_batch FROM meta.batch WHERE id_batch = p_id_batch and statut_batch = 'New')
    then
        raise EXCEPTION 'Dávka (%) neexistuje, nebo nemí ve stavu NEW', p_id_batch;
    end if;

    insert into meta.extract_summry (id_batch, name_extract, statut, insert_count, update_count,delete_count)
        VALUES (p_id_batch, xtable, 'OK', p_insert, p_update, p_delete)
    on conflict (id_batch, name_extract)
    do
        update set statut='OK', insert_count = p_insert, update_count=P_update,delete_count=p_delete;
end;

$BODY$;

COMMENT ON FUNCTION close_extract(integer, character varying, integer, integer, integer) IS 'Fce uzavírá existující extrakt v rámci dávky dávky (voláno z MongoDb)';

-- count_batch
CREATE OR REPLACE FUNCTION count_batch(
    p_table_name character varying,
    p_schema character varying,
    p_id_batch bigint,
    OUT p_count_insert integer,
    OUT p_count_upd integer)
    RETURNS record
    LANGUAGE 'plpgsql'

    COST 100
    STABLE
AS $BODY$

declare
    sql varchar(50000);
begin
    if p_schema = '' then
        p_schema = 'public';
    end if;
    -- overeni batche
    if not exists (SELECT id_batch FROM meta.batch WHERE id_batch = p_id_batch)
    then
        raise EXCEPTION 'Dávka (%) neexistuje.', p_id_batch;
    end if;

    -- overeni tabulky
    if not exists (select table_name FROM information_schema.tables where table_name = p_table_name and table_schema = p_schema)
    then
        raise EXCEPTION 'Tabulka (%) neexistuje.', p_table_name;
    end if;

    sql := 'select count(*) from '||p_table_name||' where create_batch_id = '||p_id_batch;
    begin
        execute sql WHENEVER into p_count_insert ;
    exception when others then
        p_count_insert :=0;
    end;

    sql := 'select count(*) from '||p_table_name||' where update_batch_id = '||p_id_batch;
    begin
        execute sql WHENEVER into p_count_upd ;
    exception when others then
        p_count_upd :=0;
    end;

end;

$BODY$;

COMMENT ON FUNCTION count_batch(character varying, character varying, bigint) IS 'Zjišťuje počty modifikovaných záznamů v tabulce';

-- create_batch
CREATE OR REPLACE FUNCTION create_batch(
    p_id_dataset integer)
    RETURNS bigint
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE
AS $BODY$

declare
    new_batch bigint;

begin
    -- zjištění zda existuje vstupní sada
    if not exists (select id_dataset from meta.dataset where id_dataset = p_id_dataset) then
        raise EXCEPTION 'Datová sada (%) neexistuje', p_id_dataset;
    end if;

    insert into meta.batch (id_dataset,statut_batch,start_batch)
    values (p_id_dataset,'NEW',current_Timestamp)
    returning id_batch into new_batch;

    return new_batch;
end;

$BODY$;

COMMENT ON FUNCTION create_batch(integer) IS 'Fce zakládající záznam o nové dávce. Vrací číslo dávky';

-- create_historization_process
CREATE OR REPLACE FUNCTION create_historization_process(
    p_table_name character varying)
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE
AS $BODY$

declare
    pocet integer;
    sql text;
    query_rec record;
    trg_name varchar(150);
begin

-- overerni existence tabulky
    if not exists (select table_name from information_schema.tables where upper(table_name) = upper(p_table_name) and table_schema = 'public') then
        raise EXCEPTION 'Tabulka (%) neexistuje', p_table_name;
    end if;

-- overeni existence hist tabulky
    if not exists (select table_name from information_schema.tables where upper(table_name) = upper(p_table_name)   and table_schema = 'history') then
    -- vytvoreni historicke tabulky (nexistuje)
        sql := 'create table history.'||p_table_name||' (';

        FOR  query_rec IN
            select column_name, data_type, character_maximum_length,numeric_precision,numeric_scale
            from  information_schema.columns
            where table_schema = 'public'
            and table_name = p_table_name
            order by ordinal_position
        LOOP
            sql:=sql||query_rec.column_name||' '||query_rec.data_type;
            if query_rec.character_maximum_length is not null then
                sql:=sql||'('||cast(query_rec.character_maximum_length as varchar(50))||'),';
            elsif COALESCE(query_rec.numeric_scale,0) >0 then
                sql:=sql||'('||cast(query_rec.numeric_precision as varchar(50))||','||cast(query_rec.numeric_scale as varchar(50))||'),';
            else
                sql:=sql||',';
            end if;
        END LOOP;
        sql:=sql||'operation char(1), operation_time timestamp without time zone) TABLESPACE pg_default';
        execute sql;
    else
        raise notice 'Historicka tabulka % jiz existuje', p_table_name;
    end if;
--  Vytvoření triggeru

    trg_name := 'trg_'||p_table_name;

    if not exists (select tgname from pg_trigger where tgname = trg_name) then
        sql:='CREATE TRIGGER trg_'||p_table_name||' BEFORE INSERT OR DELETE OR UPDATE ON public.'||p_table_name||' FOR EACH ROW EXECUTE PROCEDURE meta.trg_general_write_history_write_history()';
        execute sql;
    else
        raise notice 'Trigger % jiz existuje', trg_name;
    end if;

end;

$BODY$;

COMMENT ON FUNCTION create_historization_process(character varying) IS 'Vytvoření historické tabulky';


-- FUNCTION: meta.create_historization_process(character varying, character varying)
-- přetížení původní fce. Tato volaná s dvěma parametry.

CREATE OR REPLACE FUNCTION create_historization_process(
    p_schema character varying,
    p_table_name character varying)
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE
AS $BODY$

-- přetížení původní fce, přidáno schéma
declare
    pocet integer;
    sql text;
    query_rec record;
    trg_name varchar(150);
begin

-- overerni existence tabulky
    if not exists (select table_name from information_schema.tables where upper(table_name) = upper(p_table_name) and upper(table_schema) = upper(p_schema))
    then
        raise EXCEPTION 'Tabulka (%) neexistuje', p_table_name;
    end if;

-- overeni existence hist tabulky
    if not exists (select table_name from information_schema.tables where upper(table_name) = upper(p_table_name)   and table_schema = 'history')
    then
    -- vytvoreni historicke tabulky (nexistuje)
        sql := 'create table history.'||p_table_name||' (';

        FOR  query_rec IN
            select column_name, data_type, character_maximum_length,numeric_precision,numeric_scale
            from  information_schema.columns
            where table_schema = p_schema
            and table_name = p_table_name
            order by ordinal_position
        LOOP
            sql:=sql||query_rec.column_name||' '||query_rec.data_type;
            if query_rec.character_maximum_length is not null then
                sql:=sql||'('||cast(query_rec.character_maximum_length as varchar(50))||'),';
            elsif COALESCE(query_rec.numeric_scale,0) >0 then
                sql:=sql||'('||cast(query_rec.numeric_precision as varchar(50))||','||cast(query_rec.numeric_scale as varchar(50))||'),';
            else
                sql:=sql||',';
            end if;
        END LOOP;
        sql:=sql||'operation char(1), operation_time timestamp without time zone) TABLESPACE pg_default';
        execute sql;
    else
        raise notice 'Historicka tabulka % jiz existuje', p_table_name;
    end if;
--  Vytvoření triggeru

    trg_name := 'trg_'||p_table_name;

    if not exists(select tgname from pg_trigger where tgname = trg_name) then
        sql:='CREATE TRIGGER trg_'||p_table_name||' BEFORE INSERT OR DELETE OR UPDATE ON '||p_schema||'.'||p_table_name||' FOR EACH ROW EXECUTE PROCEDURE meta.trg_general_write_history()';
        execute sql;
    else
        raise notice 'Trigger % jiz existuje', trg_name;
    end if;

end;

$BODY$;

COMMENT ON FUNCTION create_historization_process(character varying, character varying) IS 'Vytvoření historické tabulky';

-- import_from_json
CREATE OR REPLACE FUNCTION import_from_json(p_batch_id bigint, p_data json, p_table_schema character varying, p_table_name character varying, p_pk json, p_sort json, p_worker_name character varying, OUT x_inserted json, OUT x_updated json)
 RETURNS record
 LANGUAGE plpgsql
AS $function$


/*
Updated by Pavel Procházka, 20.2.2020
Note:
Upraveno pořadí polí klíčů pro zápis do návratové tabulky
Updated: Pavel Procházka, 15.4.2020, zakomentování řádku debug
Updated: Pavel Procházka, 26.1.2021, odstranění temp tabulek
updated: Pavel Procházka, 6.4.2021, odstranění temp tabulek doladění
*/

declare
    pole_seznam varchar;
    pole_seznam_excluded varchar; --EXCLUDED
    pole_sort varchar;
    --pole_seznam_datatype varchar;
    pole_pk varchar;
    query_rec record;
    x_sql varchar;
    x_sql_sor varchar; -- source
begin

-- ladění
--create table if not exists tmp.debug_log (cas timestamp default now(), sql text);

if p_data::text = E'[]'::json::text then
    --insert into tmp.debug_log(SQL) VALUES(p_data);
    return;
end if;

-- vzor P_pk: ["pole1", "sloupec2" ,"dasi-sloupec"]

-- kontrola pokud je vyplněn p_sort, musí být i p_pk
if p_sort is not null and p_pk is null then
    raise EXCEPTION 'Nelze vyplnit p_sort(%) a nemít vyplněn PK', p_sort;
end if;

-- kontrola existence tabulky
    if not exists
    (
        select table_name
        from information_schema.tables
        where upper(table_name) = upper(p_table_name)
        and upper(table_schema) = upper(p_table_schema)
    )   then
        raise EXCEPTION 'Tabulka (%.%) neexistuje', p_table_schema,p_table_name;
    end if;
-- /kontrola existence tabulky
--drop table if exists pp_data;

--create temp table pp_data as
--select p_data  as data;


--execute 'select p_data as data';
-- pole z json
FOR  query_rec IN
    select json_object_keys(p_data->0) pole
LOOP
    if not exists
    (
        select column_name
        from information_schema.columns
        where upper(table_schema) = upper(p_table_schema)
        and upper(table_name) = upper(p_table_name)
        and upper (column_name) = upper(query_rec.pole)
    )   then
        raise EXCEPTION 'Tabulka %.% neobsahuje pole %', p_table_schema,p_table_name,query_rec.pole;
    else
        if pole_seznam is null then
            pole_seznam := query_rec.pole;
            pole_seznam_excluded := 'EXCLUDED.'||query_rec.pole;
        else
            pole_seznam := pole_seznam ||', '||query_rec.pole;
            pole_seznam_excluded := pole_seznam_excluded ||', '||'EXCLUDED.'||query_rec.pole;
        end if;
    end if;
end loop;
-- /pole z json

-- Kontrola PK
FOR  query_rec IN
    select cast(json_array_elements_text(p_pk) as varchar) pole order by pole
LOOP
    if not exists
    (
        select column_name
        from information_schema.columns
        where upper(table_schema) = upper(p_table_schema)
        and upper(table_name) = upper(p_table_name)
        and upper (column_name) = upper(query_rec.pole)
    )   then
        raise EXCEPTION 'Tabulka %.% neobsahuje PK pole %', p_table_schema,p_table_name,query_rec.pole;
    else
        if pole_pk is null then
            pole_pk := query_rec.pole;
        else
            pole_pk := pole_pk ||', '||query_rec.pole;
        end if;
    end if;
end loop;
-- insert into tmp.debug_log(sql) values('pole_pk: '||pole_pk);
-- pole_pk nyní obsahuje seznam pk odělený čárkami

-- /Kontrola PK

-- kontrola sort
FOR  query_rec IN
    select cast(json_array_elements_text(p_sort) as varchar) pole
LOOP
    if not exists
    (
        select column_name
        from information_schema.columns
        where upper(table_schema) = upper(p_table_schema)
        and upper(table_name) = upper(p_table_name)
        and upper (column_name) = upper(query_rec.pole)
    )   then
        raise EXCEPTION 'Tabulka %.% neobsahuje sort pole %', p_table_schema,p_table_name,query_rec.pole;
    else
        if pole_sort is null then
            pole_sort := query_rec.pole;
        else
            pole_sort := pole_sort ||', '||query_rec.pole;
        end if;
    end if;
end loop;
-- /kontrol sort
/*
drop table if exists mytemp;
x_sql := 'create temp table mytemp (';
FOR  query_rec IN
select b.pole, data_type, character_maximum_length
from (
    select
        cast(json_array_elements_text(data) as varchar) pole
        from (select p_pk as data) a
    )b
    join information_schema.columns inf
    on inf.table_schema = p_table_schema
        and inf.table_name = p_table_name
        and inf.column_name = b.pole
    order by b.pole
LOOP
    if not right(x_sql,1)='(' then
        x_sql := x_sql||',';
    end if;
    x_sql := x_sql||query_rec.pole||' '||query_rec.data_type;
    if query_rec.character_maximum_length is not null
    then
        x_sql := x_sql||'('||query_rec.character_maximum_length||')';
    end if;
end loop;
x_sql := x_sql||',upd boolean)';

insert into tmp.debug_log (sql) values (x_sql);
begin
	execute x_sql;
exception
 when others then
     raise EXCEPTION 'Chyba(1) při provádění xsql:%', x_sql;

--	insert into tmp.debug_log (sql) values ('Chyba 1:');
--	insert into tmp.debug_log (sql) values ('Chyba 1:'||x_sql);
--	insert into tmp.debug_log (sql) values ('Chyba 1:'||x_sql_sor);


	return;
end;
*/

x_sql_sor := ' select '||pole_seznam;
x_sql_sor := x_sql_sor||', $1 create_batch_id'||chr(10);
x_sql_sor := x_sql_sor||',now() created_at'||chr(10);
x_sql_sor := x_sql_sor||',$2 created_by'||chr(10);

x_sql_sor := x_sql_sor||' from (select '||chr(10);
    -- příklad:             cis_id = sor.cis_id::bigint,
    FOR  query_rec IN
        select b.pole
        ,data_type ,character_maximum_length
        from (
            select json_object_keys(p_data->0) pole
        )b
        join information_schema.columns inf
        on inf.table_schema = p_table_schema
            and inf.table_name = p_table_name
            and inf.column_name = b.pole
    LOOP
        if not right(x_sql_sor,8)='select '||chr(10) then
            x_sql_sor:= x_sql_sor||',';
        end if;
        x_sql_sor:= x_sql_sor||'cast(json_array_elements(data)->>'''||query_rec.pole||''' as '||query_rec.data_type||') "'||query_rec.pole||'"'||chr(10);
    end loop;
    if p_sort is not null then
        x_sql_sor:= x_sql_sor||', row_number() over(partition by '||pole_pk||' order by tar.'||pole_sort||') rn';
    else
        x_sql_sor:= x_sql_sor||',1 rn';
    end if;
    x_sql_sor:= x_sql_sor||' from pp_data
)a where rn = 1 ';

if p_pk is not null
then
    x_sql_sor:= x_sql_sor||chr(10)||'on conflict('||pole_pk||') do update'||chr(10);
    x_sql_sor:= x_sql_sor|| ' set ('||pole_seznam||',update_batch_id,updated_at,updated_by) ='||chr(10)||' ('||pole_seznam_excluded||',$1,now(),$2)';
end if;

-- x_sql_sor nyní obsahuje zdrojový select
-- /pouze zdroj

-- insert

x_sql:='with '||chr(10);
x_sql:=x_sql||'pp_data as (select $3  as data),'||chr(10);
x_sql:=x_sql||'rows as ('||chr(10);
x_sql:=x_sql||'insert into '||p_table_schema||'.'||p_table_name||chr(10)||' ('||pole_seznam||',create_batch_id, created_at, created_by)'||chr(10);
x_sql:=x_sql||x_sql_sor||chr(10);
x_sql:=x_sql||'RETURNING '||pole_pk||',updated_at is not null as upd)'||chr(10);
--x_sql:=x_sql||' insert into mytemp ('||pole_pk||') select '||pole_pk||' from rows '||chr(10);
--x_sql:=x_sql||' insert into mytemp  select * from rows '||chr(10);
x_sql:=x_sql||' select array_to_json(array_agg(row)) from (select * from rows) row';

--x_sql:=x_sql||'select * into tmp.mytemp from rows '||chr(10);

-- insert into tmp.debug_log (sql) values (x_sql);
begin
	execute x_sql using p_batch_id,p_worker_name ,p_data into x_inserted;
exception
 when others then
	raise EXCEPTION 'Chyba(2) při provádění xsql:%', x_sql;
	/*
	insert into tmp.debug_log (sql) values ('Chyba 2:'||x_sql);
	insert into tmp.debug_log (sql) values ('Chyba 2:'||x_sql_sor);
	*/
	return;
end;

--into x_inserted;
/*
if p_pk is not null then
        x_sql:='select array_to_json(array_agg(row)) from (select * from mytemp) row';
        --insert into tmp.debug_log (sql) values (x_sql);
        execute x_sql into x_inserted;
    end if;
*/

--drop table if exists pp_data;
--drop table if exists mytemp;

end;

$function$;

-- retention
CREATE OR REPLACE FUNCTION retention(
    tab character varying,
    col character varying,
    hours integer)
  RETURNS integer AS
$BODY$

declare
	vystup varchar;
	txt_sql varchar;
	c integer;
	cur_time timestamp;
	kontrola integer;
	dat_typ varchar;
begin

-- kontrola existence tabulky
if not exists (SELECT * FROM pg_tables WHERE tablename=tab)
then
	RAISE 'Table % does not exist.', tab USING ERRCODE = '23505';
end if;
-- kontrola na existenci sloupce
if not  exists
	(SELECT * FROM information_schema.columns
		WHERE
  		table_name   = tab
  		and column_name = col
	)
then
	RAISE 'Column % does not exist in %.', col,tab USING ERRCODE = '23505';
end if;

-- kontrola datového typu
SELECT data_type  into dat_typ FROM information_schema.columns
WHERE
	table_name   = tab
	and column_name = col;

if not(dat_typ like 'timestamp%')  then
	RAISE 'Column %(%) does not data type timestamp (%).', col,tab,dat_typ USING ERRCODE = '23505';
end if;

-- kontrola hodnoty hodin
if hours <= 0 then
	RAISE 'Hours (%) does not positive.', hours USING ERRCODE = '23505';
end if;

	cur_time = current_timestamp;
	cur_time = cur_time - hours* interval '1 hours';

	--txt_sql = 'Select count(*) from '||tab||' where '||col||'<'''||cur_time||'''';
	txt_sql = 'DELETE FROM '||tab||' where '||col||'<'''||cur_time||'''; --returning count(*)';
	execute txt_sql; --into c;
	GET DIAGNOSTICS c := ROW_COUNT;
	--select result.rowCount into c;
	return c;
end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- trg_general_write_history
CREATE OR REPLACE FUNCTION trg_general_write_history()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

declare
    sql text;
    operace varchar(1);
    ri RECORD;
    t TEXT;
    promene text := '';
    hodnoty text := '';
begin

  FOR ri IN
        SELECT ordinal_position, column_name, data_type
        FROM information_schema.columns
        WHERE
            upper(table_schema) = upper(quote_ident(TG_TABLE_SCHEMA))
        AND quote_ident(upper(table_name)) = upper(quote_ident(TG_TABLE_NAME))
        ORDER BY ordinal_position
    LOOP
        if (TG_OP = 'DELETE') then
            EXECUTE 'SELECT ($1).' || ri.column_name || '::text' INTO t USING OLD;
        else
            EXECUTE 'SELECT ($1).' || ri.column_name || '::text' INTO t USING NEW;
        end if;
        promene:=promene||ri.column_name||', ';
        if t is null then
            hodnoty:=hodnoty||'null, ';
        else
            hodnoty:=hodnoty||''''||coalesce(t, '')||''', ';
        end if;

    END LOOP;

    promene:=promene||'operation,operation_time';

if (TG_OP = 'DELETE') THEN
    operace='D';
elsif (TG_OP = 'UPDATE') THEN
    operace='U';
elsif (TG_OP = 'INSERT') THEN
    operace='I';
else
    operace='?';
end if;

hodnoty:=hodnoty||''''||operace||''','''||cast(now() as varchar(25))||'''';

execute 'INSERT INTO history.'||TG_TABLE_NAME||'('||promene||') values ('||hodnoty||')';

IF (TG_OP = 'DELETE') THEN
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            RETURN NEW;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
end;

$BODY$;
