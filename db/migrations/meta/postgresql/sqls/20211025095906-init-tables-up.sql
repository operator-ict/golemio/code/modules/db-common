-- db_type
CREATE SEQUENCE IF NOT EXISTS db_type_id_db_seq;
CREATE TABLE IF NOT EXISTS db_type (
    id_db integer DEFAULT nextval('db_type_id_db_seq'::regclass) NOT NULL,
    name_db character varying(150),
    created_at timestamp without time zone DEFAULT now(),

    CONSTRAINT db_type_name UNIQUE (name_db),
    CONSTRAINT db_type_pkey PRIMARY KEY (id_db)
);

COMMENT ON TABLE db_type IS 'seznam použitých databází';
COMMENT ON COLUMN db_type.id_db IS 'id databáze';
COMMENT ON COLUMN db_type.name_db IS 'Název db (unikátní)';
COMMENT ON CONSTRAINT db_type_name ON db_type IS 'unikátní název';

-- dataset
CREATE SEQUENCE IF NOT EXISTS dataset_id_dataset_seq;
CREATE TABLE IF NOT EXISTS dataset (
    id_dataset integer DEFAULT nextval('dataset_id_dataset_seq'::regclass) NOT NULL,
    code_dataset character varying(50) NOT NULL,
    name_dataset character varying(250) NOT NULL,
    description text,
    retention_days integer,
    h_retention_days integer,
    created_at timestamp without time zone DEFAULT now(),

    CONSTRAINT dataset_code UNIQUE (code_dataset),
    CONSTRAINT dataset_name UNIQUE (name_dataset),
    CONSTRAINT dataset_pkey PRIMARY KEY (id_dataset)
);

COMMENT ON TABLE dataset IS 'Datová sada';
COMMENT ON COLUMN dataset.id_dataset IS 'id datové sady';
COMMENT ON COLUMN dataset.code_dataset IS 'Zkrácený název datové sady, tento kód bude použit při tvorbě názvu tabulky jako prefix (unikátní)';
COMMENT ON COLUMN dataset.name_dataset IS 'Název datové sady (unikátní)';
COMMENT ON COLUMN dataset.description IS 'Popis datové sady';
COMMENT ON COLUMN dataset.retention_days IS 'Retence datové sady ve dnech. NULL nebo 0 znamená bez retence';
COMMENT ON COLUMN dataset.h_retention_days IS 'Retence datové sady(historické tabulky) ve dnech. NULL nebo 0 znamená bez retence';
COMMENT ON CONSTRAINT dataset_code ON dataset IS 'Unikátní code';
COMMENT ON CONSTRAINT dataset_name ON dataset IS 'Unikátní jméno datové sady';

CREATE TRIGGER trg_dataset BEFORE INSERT OR DELETE OR UPDATE ON dataset FOR EACH ROW EXECUTE FUNCTION trg_general_write_history();

-- extract
CREATE TABLE IF NOT EXISTS "extract" (
    name_extract character varying(150) NOT NULL,
    id_dataset integer NOT NULL,
    id_db integer NOT NULL,
    description text,
    schema_extract character varying(150) DEFAULT 'public'::character varying,
    retention_days integer,
    h_retention_days integer,
    created_at timestamp without time zone DEFAULT now(),

    CONSTRAINT extract_pkey PRIMARY KEY (name_extract),
    CONSTRAINT extract_dataset FOREIGN KEY (id_dataset) REFERENCES dataset(id_dataset),
    CONSTRAINT extract_db FOREIGN KEY (id_db) REFERENCES db_type(id_db)
);

COMMENT ON TABLE "extract" IS 'Tabulky (kolekece) datové sady';
COMMENT ON COLUMN "extract".name_extract IS 'Názeva tabulky/kolekce';
COMMENT ON COLUMN "extract".id_dataset IS 'FK do db_dataset';
COMMENT ON COLUMN "extract".id_db IS 'typ databáze';
COMMENT ON COLUMN "extract".retention_days IS 'Retence extraktu, pokud je odlišná od retence datové sady ve dnech. NULL znamená, že bude použita retence sady, 0 znamená, že extrakt bude bez retence bez ohledu na na stavení retence sady';
COMMENT ON COLUMN "extract".h_retention_days IS 'Retence extraktu (historická tabulka), pokud je odlišná od historické retence datové sady ve dnech. NULL znamená, že bude použita retence sady, 0 znamená, že extrakt bude bez retence bez ohledu na na stavení retence sady';

CREATE TRIGGER trg_extract BEFORE INSERT OR DELETE OR UPDATE ON "extract" FOR EACH ROW EXECUTE FUNCTION trg_general_write_history();

COMMENT ON CONSTRAINT extract_dataset ON "extract" IS 'Vazba na dataset';
COMMENT ON CONSTRAINT extract_db ON "extract" IS 'Vazby na typ databáze';

-- batch
CREATE SEQUENCE IF NOT EXISTS batch_id_batch_seq;
CREATE TABLE IF NOT EXISTS batch (
    id_batch bigint DEFAULT nextval('batch_id_batch_seq'::regclass) NOT NULL,
    id_dataset integer NOT NULL,
    statut_batch character varying(20) DEFAULT 'New'::character varying NOT NULL,
    start_batch timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    end_batch timestamp without time zone,
    created_at timestamp without time zone DEFAULT now(),

    CONSTRAINT batch_pkey PRIMARY KEY (id_batch),
    CONSTRAINT batch_dataset FOREIGN KEY (id_dataset) REFERENCES dataset(id_dataset)
);

COMMENT ON TABLE batch IS 'Datová dávka';
COMMENT ON COLUMN batch.id_dataset IS 'id datové sady';
COMMENT ON COLUMN batch.statut_batch IS 'Stav dávky';
COMMENT ON COLUMN batch.end_batch IS 'Ukončení zpracování';

-- batch_validation
CREATE SEQUENCE IF NOT EXISTS batch_validation_id_seq;
CREATE TABLE IF NOT EXISTS batch_validation (
    id bigint DEFAULT nextval('batch_validation_id_seq'::regclass) NOT NULL,
    dataset bigint NOT NULL,
    id_batch bigint NOT NULL,
    key character varying(255),
    type character varying(255),
    value character varying(255),

    CONSTRAINT batch_validation_pkey PRIMARY KEY (id)
);

-- extract_summry
CREATE TABLE IF NOT EXISTS extract_summry (
    id_batch bigint NOT NULL,
    name_extract character varying(50) NOT NULL,
    statut character varying(20) NOT NULL,
    insert_count integer,
    update_count integer,
    delete_count integer,
    created_at timestamp without time zone DEFAULT now(),

    CONSTRAINT sumext_pkey PRIMARY KEY (id_batch, name_extract),
    CONSTRAINT sumext_batch FOREIGN KEY (id_batch) REFERENCES batch(id_batch),
    CONSTRAINT sumext_extname FOREIGN KEY (name_extract) REFERENCES "extract"(name_extract)
);

COMMENT ON TABLE extract_summry IS 'Počty záznamů v dávce a extractu';
COMMENT ON COLUMN extract_summry.id_batch IS 'id dávky';
COMMENT ON COLUMN extract_summry.name_extract IS 'Název extractu (FK)';
