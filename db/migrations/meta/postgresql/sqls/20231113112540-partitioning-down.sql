DROP PROCEDURE create_monthly_partitions(table_name text, date_from date, date_to date);
DROP PROCEDURE create_yearly_partitions(table_name text, date_from date, date_to date);
DROP PROCEDURE rename_partitioned_table(table_name text, table_name_new text);
