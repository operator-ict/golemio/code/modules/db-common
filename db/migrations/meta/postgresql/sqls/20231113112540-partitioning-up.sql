CREATE OR REPLACE PROCEDURE create_monthly_partitions(table_name text, date_from date, date_to date)
LANGUAGE plpgsql
AS $$
DECLARE
    current_month date;
BEGIN
    EXECUTE format('CREATE TABLE %I PARTITION OF %I FOR VALUES FROM (MINVALUE) TO (%L::timestamp)', table_name || '_min', table_name, date_from);

    FOR current_month IN (SELECT generate_series(date_from, date_to - interval '1 month', '1 month'::interval)) LOOP
        EXECUTE format(
            'CREATE TABLE %I PARTITION OF %I FOR VALUES FROM (%L::timestamp) TO (%L::timestamp)',
            table_name || '_y' || to_char(current_month, 'YYYY') || '_m' || to_char(current_month, 'MM'),
            table_name,
            current_month,
            current_month + interval '1 month'
        );
    END LOOP;

    EXECUTE format('CREATE TABLE %I PARTITION OF %I FOR VALUES FROM (%L::timestamp) TO (MAXVALUE)', table_name || '_y' || to_char(date_to, 'YYYY') || 'max', table_name, (date_to + interval '1 day'));
END;
$$;

COMMENT ON PROCEDURE create_monthly_partitions IS '
Creates monthly partitions on interval specified by date_from and date_to and also (min,date_from) & (date_to,max) partitions outside of the interval.
';


CREATE OR REPLACE PROCEDURE create_yearly_partitions(table_name text, date_from date, date_to date)
LANGUAGE plpgsql
AS $$
DECLARE
    current_year date;
BEGIN
    EXECUTE format('CREATE TABLE %I PARTITION OF %I FOR VALUES FROM (MINVALUE) TO (%L::timestamp)', table_name || '_min', table_name, date_from);

    FOR current_year IN (SELECT generate_series(date_from, date_to - interval '1 year', '1 year'::interval)) LOOP
        EXECUTE format(
            'CREATE TABLE %I PARTITION OF %I FOR VALUES FROM (%L::timestamp) TO (%L::timestamp)',
            table_name || '_y' || to_char(current_year, 'YYYY'),
            table_name,
            current_year,
            current_year + interval '1 year'
        );
    END LOOP;

    EXECUTE format('CREATE TABLE %I PARTITION OF %I FOR VALUES FROM (%L::timestamp) TO (MAXVALUE)', table_name || '_y' || to_char(date_to, 'YYYY') || 'max', table_name, (date_to + interval '1 day'));
END;
$$;

COMMENT ON PROCEDURE create_yearly_partitions IS '
Creates yearly partitions on interval specified by date_from and date_to and also (min,date_from) & (date_to,max) partitions outside of the interval.
';


CREATE OR REPLACE PROCEDURE rename_partitioned_table(table_name text, table_name_new text)
LANGUAGE plpgsql
AS $$
DECLARE
    partition_name text;
    new_partition_name text;
BEGIN
    -- rename table's partitions
    FOR partition_name IN (
        SELECT
            p.relname AS p_name
        FROM pg_inherits i
        INNER JOIN pg_class c ON i.inhparent = c.oid
        INNER JOIN pg_class p ON i.inhrelid = p.oid
        INNER JOIN pg_namespace n ON c.relnamespace = n.oid
        WHERE c.relkind = 'p' AND  c.relname = table_name AND n.nspname = current_schema
    )
    LOOP
        -- check if the partition_name contains the table_name
        IF position(table_name IN partition_name) = 0 THEN
            RAISE EXCEPTION 'The table_name does not exist in partition_name.';
        END IF;
        -- create the new partition name
        new_partition_name := REPLACE(partition_name, table_name, table_name_new);

        -- rename partition
        EXECUTE format('ALTER TABLE %I RENAME TO %I', partition_name, new_partition_name);
    END LOOP;

    -- rename table
    EXECUTE format('ALTER TABLE %I RENAME TO %I', table_name, table_name_new);
END $$;

COMMENT ON PROCEDURE rename_partitioned_table IS '
Renames table and all of its partitions by replacing table_name with table_name_new
';
