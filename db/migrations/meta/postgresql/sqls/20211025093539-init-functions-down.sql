DROP FUNCTION IF EXISTS add_audit_fields;

DROP FUNCTION IF EXISTS close_batch;

DROP FUNCTION IF EXISTS close_extract;

DROP FUNCTION IF EXISTS count_batch;

DROP FUNCTION IF EXISTS create_batch;

DROP FUNCTION IF EXISTS create_historization_process(varchar);
DROP FUNCTION IF EXISTS create_historization_process(varchar,varchar);

DROP FUNCTION IF EXISTS import_from_json;

DROP FUNCTION IF EXISTS retention;

DROP FUNCTION IF EXISTS trg_general_write_history;
