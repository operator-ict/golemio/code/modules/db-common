# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1.15] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.1.4] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.1.3] - 2023-11-27

### Added

-   Add generic partitioning procedures

## [1.1.2] - 2023-09-27

### Added

-   Add city district migrations ([city-districts#8](https://gitlab.com/operator-ict/golemio/code/modules/city-districts/-/issues/8))

### Changed

-   Move intermodal migration to new traffic-common module

## [1.1.1] - 2023-05-03

### Removed

-   Drop the `public.error_log` table ([core#61](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/61))

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.2] - 2022-10-11

### Changed

-   Change error_log message type to text

## [1.0.1] - 2022-05-18

### Added

-   Add public.rsd_tmc_osm_mapping which is used in FCD

## [1.0.0] - 2021-11-23

### Added

-   Create module for shared migrations ([db-common#1](https://gitlab.com/operator-ict/golemio/code/modules/db-common/-/issues/1))

